package controllers.model;

public class WorldBox extends GameObject{

    
    public WorldBox(final Pos2d center, final double edge) {
        super(center, new Speed2d(0,0), 0, edge);
    }
    
    @Override
    public Pos2d getBotLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
    }

    @Override
    public Pos2d getTopLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
    }

    @Override
    public Pos2d getTopRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
    }

    @Override
    public Pos2d getBotRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
    }

}
