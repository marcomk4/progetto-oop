package controllers.model;

public class Speed2d {

    private double x,y;

    public Speed2d(double x,double y){
        this.x=x;
        this.y=y;
    }

    public Speed2d sum(Speed2d s){
        return new Speed2d(x+s.x,y+s.y);
    }

    public double module(){
        return (double)Math.sqrt(x*x+y*y);
    }

    public Speed2d getNormalized(){
        double module=(double)Math.sqrt(x*x+y*y);
        return new Speed2d(x/module,y/module);
    }

    public Speed2d mul(double change){
        return new Speed2d(x*change,y*change);
    }

    public String toString(){
        return "Speed2d("+x+","+y+")";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
