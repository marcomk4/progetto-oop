package controllers.model;

public class Ball extends GameObject{
    
    public Ball(final Pos2d center, final Speed2d speed, final double acc, final double edge) {
        super(center, speed, acc, edge);
    }

    @Override
    public Pos2d getBotLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
    }

    @Override
    public Pos2d getTopLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
    }

    @Override
    public Pos2d getTopRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
    }

    @Override
    public Pos2d getBotRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
    }
    
    public void flipSpeedOnX() {
        super.setSpeed(new Speed2d(- super.getCurrentSpeed().getX(),
                super.getCurrentSpeed().getY()));
    }
    
    public void flipSpeedOnY() {
        super.setSpeed(new Speed2d(super.getCurrentSpeed().getX(),
                - super.getCurrentSpeed().getY()));
    }

}
