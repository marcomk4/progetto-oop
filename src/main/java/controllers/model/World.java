package controllers.model;

import java.util.ArrayList;
import java.util.List;

import controllers.model.Brick.DirCol;

public class World {

    private final List<Ball> balls;
    private final List<Brick> bricks;
    private Ship ship;
    private final WorldBox worldBox;
    
    public World(final WorldBox worldBox){
        this.balls = new ArrayList<>();
        this.bricks = new ArrayList<>();
        this.worldBox = worldBox;
    }

    public void setShip(Ship s) {
        this.ship = s;
        this.bricks.add(s);
    }
    public void setBall(Ball ba){
        this.balls.add(ba);
    }
    
    
    public void addBrick(Brick br) {
        this.bricks.add(br);
    }
    
    public void updateState(int delta){
        bricks.stream().forEach(br -> br.updateState(delta));
        balls.stream().forEach(ba -> ba.updateState(delta));
        checkBoundaries();
        checkCollisions();
    }

    private void checkBoundaries(){

        final double wTop = worldBox.getTopLeftPos().getY();
        final double wBot = worldBox.getBotLeftPos().getY();
        final double wRight = worldBox.getBotRightPos().getX();
        final double wLeft = worldBox.getBotLeftPos().getX();

        final double shipPosX = ship.getCurrentPos().getX();
        final double edge = ship.getEdge();
        if (shipPosX + edge > wRight) {
            ship.setPos(new Pos2d(wRight - edge, ship.getCurrentPos().getY()));

        } else if (shipPosX - edge < wLeft) {
            ship.setPos(new Pos2d(wLeft + edge, ship.getCurrentPos().getY()));

        }
        
        for (final Ball ba : balls) {
            final Pos2d bCurrentPos = ba.getCurrentPos();

            final double ballTop = ba.getTopLeftPos().getY();
            final double ballBot = ba.getBotLeftPos().getY();
            final double ballRight = ba.getBotRightPos().getX();
            final double ballLeft = ba.getBotLeftPos().getX();

            if (ballTop > wTop) {
                ba.setPos(new Pos2d(bCurrentPos.getX(), wTop - ba.getEdge()));
                ba.flipSpeedOnY();
            } else if (ballBot < wBot) {
                ba.setPos(new Pos2d(bCurrentPos.getX(), wBot + ba.getEdge()));
                ba.flipSpeedOnY();
            }
            if (ballRight > wRight) {
                ba.setPos(new Pos2d(wRight - ba.getEdge(), bCurrentPos.getY()));
                ba.flipSpeedOnX();
            } else if (ballLeft < wLeft) {
                ba.setPos(new Pos2d(wLeft + ba.getEdge(), bCurrentPos.getY()));
                ba.flipSpeedOnX();
            }
        }
    }
    
    private void checkCollisions() {

        for (final Ball ba : balls) {
            final Pos2d bCurrentPos = ba.getCurrentPos();
            final double ballTop = ba.getTopLeftPos().getY();
            final double ballBot = ba.getBotLeftPos().getY();
            final double ballRight = ba.getBotRightPos().getX();
            final double ballLeft = ba.getBotLeftPos().getX();
            
            Brick found = null;
            for (Brick br : bricks) {
                final double brTop = br.getTopLeftPos().getY();
                final double brBot = br.getBotLeftPos().getY();
                final double brRight = br.getBotRightPos().getX();
                final double brLeft = br.getBotLeftPos().getX();
                
                boolean checkLeft = false;
                boolean checkRight = false;
                boolean checkBot = false;
                boolean checkTop = false;

                if (ballLeft <= brRight) {
                    checkLeft = true;
                } else {
                    br.setDirCol(DirCol.LEFT);
                }
                if (ballRight >= brLeft) {
                    checkRight = true;
                } else {
                    br.setDirCol(DirCol.RIGHT);
                }
                if (ballBot <= brTop) {
                    checkBot = true;
                } else {
                    br.setDirCol(DirCol.BOTTOM);
                }
                if (ballTop >= brBot) {
                    checkTop = true;
                } else {
                    br.setDirCol(DirCol.TOP);
                }
                if (checkLeft && checkRight && checkBot && checkTop) {
                    found = br;

                    switch (br.getDirCol()) {
                    case LEFT:
                        ba.setPos(new Pos2d(brRight + ba.getEdge(), bCurrentPos.getY()));
                        ba.flipSpeedOnX();
                        break;
                    case RIGHT:
                        ba.setPos(new Pos2d(brLeft - ba.getEdge(), bCurrentPos.getY()));
                        ba.flipSpeedOnX();
                        break;
                    case BOTTOM:
                        ba.setPos(new Pos2d(bCurrentPos.getX(), brTop + ba.getEdge()));
                        ba.flipSpeedOnY();
                        break;
                    case TOP:
                        ba.setPos(new Pos2d(bCurrentPos.getX(), brBot - ba.getEdge()));
                        ba.flipSpeedOnY();
                        break;
                    default:
                        break;
                    }

                }
            }

            if (found != null) {
                if (!found.equals(this.getShip())) {
                    bricks.remove(found);
                }

            }
        }

    }

    public List<GameObject> getSceneEntities(){
        List<GameObject> entities = new ArrayList<GameObject>();
        entities.addAll(this.balls);
        entities.addAll(this.bricks);
        return entities;
    }

    public WorldBox getWorldBox(){
        return this.worldBox;
    }
    
    public List<Ball> getBall(){
        return this.balls;
    }
    
    public Ship getShip() {
        return this.ship;
    }
}
