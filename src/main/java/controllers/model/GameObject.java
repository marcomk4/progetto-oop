package controllers.model;

public abstract class GameObject {
    
    private Pos2d center;
    private Speed2d speed;
    private double acc;
    private double edge;
    
    public GameObject(final Pos2d center, final Speed2d speed, final double acc, final double edge) {
        this.center = center;
        this.speed = speed;
        this.acc = acc;
        this.edge = edge;
    }
    
    public Pos2d getCurrentPos() {
        return this.center;
    }
    
    public Speed2d getCurrentSpeed() {
        return this.speed;
    }
    
    public double getCurrentAcc() {
        return this.acc;
    }
    
    public double getEdge() {
        return this.edge;
    }
    
    public void setPos(final Pos2d pos) {
        this.center = pos;
    }
    
    public void setSpeed(final Speed2d speed) {
        this.speed = speed;
    }
    
    public void setAcc(final double acc) {
        this.acc = acc;
    }
    
    public void updateState(final int delta) {
        this.center = this.center.sum(speed.mul(acc*delta*0.001));
    }
    
    public abstract Pos2d getBotLeftPos();
    public abstract Pos2d getTopLeftPos();
    public abstract Pos2d getTopRightPos();
    public abstract Pos2d getBotRightPos();

}
