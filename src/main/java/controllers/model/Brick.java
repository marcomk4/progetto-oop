package controllers.model;

public class Brick extends GameObject{
    
    enum DirCol {
        TOP, LEFT, BOTTOM, RIGHT;
    }
    
    private DirCol dirCol;
    
    public Brick(final Pos2d center, final double edge) {
        super(center, new Speed2d(0,0), 0, edge);
    }

    @Override
    public Pos2d getBotLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge() / 2);
    }

    @Override
    public Pos2d getTopLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge() / 2);
    }

    @Override
    public Pos2d getTopRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge() / 2);
    }

    @Override
    public Pos2d getBotRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge() / 2);
    }
    
    public DirCol getDirCol() {
        return this.dirCol;
    }
    
    public void setDirCol(final DirCol d) {
        this.dirCol = d;
    }

}
