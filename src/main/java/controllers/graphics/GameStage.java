package controllers.graphics;

import java.util.List;

import controllers.input.*;
import controllers.model.*;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class GameStage extends Stage{
    private final World world;
    private final InitSceneUI root;
    private boolean flagLaunch;
    private boolean flagRight;
    private boolean flagLeft;
    private UIController controller;

    public GameStage(final World world, final double screenWidth, final double screenHeight, final double width, final double height) {
        this.world = world;
        this.root = new InitSceneUI(screenWidth,screenHeight,width,height);
        Scene scene = new Scene(root, screenWidth, screenHeight);
        this.setTitle("ARKANOID");
        this.setScene(scene);
        this.show();

        this.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(-1);
            }
           });

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.SPACE) {
                    flagLaunch = true;
                }else if (event.getCode() == KeyCode.RIGHT) {
                    flagRight = true;
                }else if (event.getCode() == KeyCode.LEFT) {
                    flagLeft = true;
                }
            }

        });

        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.SPACE) {
                    flagLaunch = false;
                }else if (event.getCode() == KeyCode.RIGHT) {
                    flagRight = false;
                }else if (event.getCode() == KeyCode.LEFT) {
                    flagLeft = false;
                }
            }

        });
    }
    
    public void update() {
        if (flagLaunch == true) {
            controller.notifyCommand(new LaunchBall());
        }else if (flagRight == true) {
            controller.notifyCommand(new MoveRight());
        }else if (flagLeft == true) {
            controller.notifyCommand(new MoveLeft());
        }
    }
    
    public void render() {
        this.root.drawWorld();
    }
    
    public void setInputController(UIController c){
        controller = c;
    }
    
    public class InitSceneUI extends Group {
        private GraphicsContext gc;
        private final double screenWidth;
        private final double screenHeight;
        private final double centerX;
        private final double centerY;
        private final double ratioX;
        private final double ratioY;
        
        public InitSceneUI(final double screenWidth, final double screenHeight, final double width, final double height) {
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            final Canvas canvas = new Canvas (screenWidth, screenHeight);
            this.gc = canvas.getGraphicsContext2D();
            this.getChildren().add(canvas);
            this.centerX = screenWidth / 2;
            this.centerY = screenHeight / 2;
            this.ratioX = screenWidth / width;
            this.ratioY = screenHeight / height;
        }
        
        private int getPixelX(final Pos2d pos) {
            return (int) Math.round(centerX + pos.getX() * ratioX);
        }
        
        private int getPixelY(final Pos2d pos) {
            return (int) Math.round(centerY - pos.getY() * ratioY);
        }
        
        private void drawWorld() {
            
            gc.clearRect(0, 0, this.screenWidth, this.screenHeight);
            List<GameObject> gameEntities = world.getSceneEntities();
            
            WorldBox worldBox = world.getWorldBox();
            int x0 = getXinPixel(worldBox.getTopLeftPos());
            int y0 = getYinPixel(worldBox.getTopLeftPos());
            int x1 = getXinPixel(worldBox.getBotRightPos());
            int y1 = getYinPixel(worldBox.getBotRightPos());

            gc.strokeRect(x0, y0, x1-x0, y1-y0);
            
            gameEntities.stream().forEach(e -> {
                Pos2d position = e.getCurrentPos();
                int x = getXinPixel(position);
                int y = getYinPixel(position);
                if (e instanceof Ball) {
                    Ball ba = (Ball) e;
                    int edge = getDeltaXinPixel(ba.getEdge());
                    gc.fillOval(x-edge, y-edge, edge*2, edge*2);
                }else if (e instanceof Brick) {
                    Brick br = (Brick) e;
                    int edge = getDeltaXinPixel(br.getEdge());
                    gc.fillRect(x-edge, y-edge/2, edge*2, edge);
                }

            });
        }
        
        private int getXinPixel(final Pos2d pos) {
            return (int) Math.round(centerX + pos.getX() * ratioX);
        }
        
        private int getYinPixel(final Pos2d pos) {
            return (int) Math.round(centerY - pos.getY() * ratioY);
        }
        
        private int getDeltaXinPixel(final double dx) {
            return (int) Math.round(dx * ratioX);
        }

    }
}
