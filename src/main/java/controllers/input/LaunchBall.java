package controllers.input;

import java.util.List;

import controllers.model.*;

public class LaunchBall implements Command{

    @Override
    public void execute(World world, int delta) {
        List<Ball> ball = world.getBall();
        ball.stream().forEach(b -> {
            if (b.getCurrentAcc() == 0) {
                b.setAcc(5);
            }
        });
    }

}
