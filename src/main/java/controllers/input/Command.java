package controllers.input;

import controllers.model.World;

public interface Command {

    void execute(World scene, int delta);
}
