package controllers.input;

import java.util.List;

import controllers.model.*;

public class MoveRight implements Command{

    @Override
    public void execute(World world, int delta) {
        List<Ball> ball = world.getBall();
        Ship s = world.getShip();
        s.setPos(new Pos2d(s.getCurrentPos().getX() + 10 * delta * 0.001, s.getCurrentPos().getY()));
        ball.stream().forEach(b -> {
            if (b.getCurrentAcc() == 0) {
                if (b.getCurrentAcc() == 0) {
                    b.setPos(new Pos2d(b.getCurrentPos().getX() + 10 * delta * 0.001, b.getCurrentPos().getY()));
                }
            }
        });
    }

}
