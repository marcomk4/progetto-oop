package controllers.core;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import application.Main;
import controllers.graphics.GameStage;
import controllers.input.*;
import controllers.model.*;

public class GameEngine extends Thread implements UIController{
    
    private static final double SPACING = 2.5;
    private long period = 20;
    private World world;
    private GameStage stage;
    private BlockingQueue<Command> cmdQueue; 
    
    public GameEngine() {
        cmdQueue = new ArrayBlockingQueue<Command>(100);
    }
    
    public void setup(final Main stage, final double screenWidth, final double screenHeight) {
        this.world = new World(new WorldBox(new Pos2d(0,0),8));
        this.world.setBall(new Ball(new Pos2d(0,-6), new Speed2d(1,1.5), 0, 0.2));
        this.world.setShip(new Ship(new Pos2d(0,-7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6,7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING,7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 2,7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 3,7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 4,7), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 5,7), 1));

        this.world.addBrick(new Brick(new Pos2d(-6,5.5), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING,5.5), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 2,5.5), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 3,5.5), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 4,5.5), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 5,5.5), 1));
        
        this.world.addBrick(new Brick(new Pos2d(-6,4), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING,4), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 2,4), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 3,4), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 4,4), 1));
        this.world.addBrick(new Brick(new Pos2d(-6 + SPACING * 5,4), 1));
        
        
        this.stage = new GameStage(world, screenWidth,screenHeight, 20, 20);
        this.stage.setInputController(this);
    }
    
    public void run() {
        long previousTime = System.currentTimeMillis();
        while(true) {
            long currentTime = System.currentTimeMillis();
            int timeElapsed = (int) (currentTime - previousTime);
            processInput(timeElapsed);
            updateGame(timeElapsed);
            render();
            waitForNextFrame(currentTime);
            previousTime = currentTime;
        }
        
    }
    
    protected void processInput(final int timeElapsed) {
        Command cmd = cmdQueue.poll();
        if (cmd != null){
            cmd.execute(world, timeElapsed);
        }
    }
    
    protected void updateGame(final int timeElapsed) {
        stage.update();
        world.updateState(timeElapsed);
    }
    
    protected void render() {
        stage.render();
    }
    
    public void waitForNextFrame(final long currentTime) {
        long lag = System.currentTimeMillis() - currentTime;
        if (lag < period) {
            try {
                Thread.sleep(period-lag);
            } catch (Exception ex){}
        }
    }

    @Override
    public void notifyCommand(Command cmd) {
        cmdQueue.add(cmd);
        
    }
}


